/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __c7f8370f6fc346044ca1cdefa639ecfb__
#define __c7f8370f6fc346044ca1cdefa639ecfb__

#include "ui_main_window.h"

class MainWindow : public QMainWindow, private Ui_MainWindow
{
	Q_OBJECT

public:
	MainWindow(bool portrait, bool landscape);
	~MainWindow();

private:
	QSize m_GameSize;
	QSize m_ViewSize;
	QImage m_Image;

	void setGameSize(const QSize & size);
	void setViewSize(const QSize & size);

	Q_SLOT void on_uiWidthSpinBox_valueChanged(int);
	Q_SLOT void on_uiHeightSpinBox_valueChanged(int);
	Q_SLOT void on_uiResolutionCombo_currentIndexChanged(int index);
	Q_SLOT void on_uiOpenButton_clicked();

	void updateImage();
};

#endif
