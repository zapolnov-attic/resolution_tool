/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include <QApplication>
#include <QResource>
#include "main_window.h"

int main(int argc, char ** argv)
{
	QApplication app(argc, argv);

	Q_INIT_RESOURCE(resources);

	app.setOrganizationName("FriedCroc");
	app.setOrganizationDomain("friedcroc.com");

	bool portrait = false, landscape = false;
  #ifdef PORTRAIT
	app.setApplicationName("Resolution Tool Portrait");
	portrait = true;
  #else
	app.setApplicationName("Resolution Tool Landscape");
	landscape = true;
  #endif

	MainWindow * mainWindow = new MainWindow(portrait, landscape);
	mainWindow->show();

	return app.exec();
}
