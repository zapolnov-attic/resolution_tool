/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "main_window.h"
#include "custom_resolution_dialog.h"
#include "math/scale.h"
#include <QFileDialog>
#include <QResizeEvent>
#include <QSettings>
#include <QMessageBox>

MainWindow::MainWindow(bool portrait, bool landscape)
{
	setupUi(this);

	if (portrait)
		setWindowTitle(tr("%1 - Portrait").arg(windowTitle()));
	else
		setWindowTitle(tr("%1 - Landscape").arg(windowTitle()));

	uiScreenFrame->raise();
	uiInnerFrame->raise();

	uiResolutionCombo->addItem(tr("======== iOS ========"));
	if (portrait) uiResolutionCombo->addItem(tr("320x480 (iPhone 1-3, 2:3)"), QSize(320, 480));
	if (landscape) uiResolutionCombo->addItem(tr("480x320 (iPhone 1-3, 3:2)"), QSize(480, 320));
	if (portrait) uiResolutionCombo->addItem(tr("640x960 (iPhone 4, 2:3)"), QSize(640, 960));
	if (landscape) uiResolutionCombo->addItem(tr("960x640 (iPhone 4, 3:2)"), QSize(960, 640));
	if (portrait) uiResolutionCombo->addItem(tr("640x1136 (iPhone 5, 9:16)"), QSize(640, 1136));
	if (landscape) uiResolutionCombo->addItem(tr("1136x640 (iPhone 5, 16:9)"), QSize(1136, 640));
	if (portrait) uiResolutionCombo->addItem(tr("768x1024 (iPad, 3:4)"), QSize(768, 1024));
	if (landscape) uiResolutionCombo->addItem(tr("1024x768 (iPad, 4:3)"), QSize(1024, 768));
	if (portrait) uiResolutionCombo->addItem(tr("1536x2048 (iPad retina, 3:4)"), QSize(1536, 2048));
	if (landscape) uiResolutionCombo->addItem(tr("2048x1536 (iPad retina, 4:3)"), QSize(2048, 1536));

	uiResolutionCombo->addItem(tr("====== Android ======"));
	if (portrait) uiResolutionCombo->addItem(tr("240x320 (QVGA, 120 dpi)"), QSize(240, 320));
	if (landscape) uiResolutionCombo->addItem(tr("320x240 (QVGA, 120 dpi)"), QSize(320, 240));
	if (portrait) uiResolutionCombo->addItem(tr("240x400 (WQVGA400, 120 dpi)"), QSize(240, 400));
	if (landscape) uiResolutionCombo->addItem(tr("400x240 (WQVGA400, 120 dpi)"), QSize(400, 240));
	if (portrait) uiResolutionCombo->addItem(tr("240x432 (WQVGA432, 120 dpi)"), QSize(240, 432));
	if (landscape) uiResolutionCombo->addItem(tr("432x240 (WQVGA432, 120 dpi)"), QSize(432, 240));
	if (portrait) uiResolutionCombo->addItem(tr("320x480 (HVGA, 160 dpi)"), QSize(320, 480));
	if (landscape) uiResolutionCombo->addItem(tr("480x320 (HVGA, 160 dpi)"), QSize(480, 320));
	if (portrait) uiResolutionCombo->addItem(tr("480x640 (240 dpi)"), QSize(480, 640));
	if (landscape) uiResolutionCombo->addItem(tr("640x480 (240 dpi)"), QSize(640, 480));
	if (portrait) uiResolutionCombo->addItem(tr("480x800 (WVGA800, 120/160/240 dpi)"), QSize(480, 800));
	if (landscape) uiResolutionCombo->addItem(tr("800x480 (WVGA800, 120/160/240 dpi)"), QSize(800, 480));
	if (portrait) uiResolutionCombo->addItem(tr("480x854 (WVGA854, 120/160/240 dpi)"), QSize(480, 854));
	if (landscape) uiResolutionCombo->addItem(tr("854x480 (WVGA854, 120/160/240 dpi)"), QSize(854, 480));
	if (portrait) uiResolutionCombo->addItem(tr("600x1024 (160/240 dpi)"), QSize(600, 1024));
	if (landscape) uiResolutionCombo->addItem(tr("1024x600 (120/160/240 dpi)"), QSize(1024, 600));
	if (portrait) uiResolutionCombo->addItem(tr("640x960 (320 dpi)"), QSize(640, 960));
	if (landscape) uiResolutionCombo->addItem(tr("960x640 (320 dpi)"), QSize(960, 640));
	if (landscape) uiResolutionCombo->addItem(tr("1280x800 (WXGA, 160 dpi)"), QSize(1280, 800));
	if (landscape) uiResolutionCombo->addItem(tr("1024x768 (160 dpi)"), QSize(1024, 768));
	if (landscape) uiResolutionCombo->addItem(tr("1280x768 (160 dpi)"), QSize(1280, 768));
	if (landscape) uiResolutionCombo->addItem(tr("1536x1152 (240 dpi)"), QSize(1536, 1152));
	if (landscape) uiResolutionCombo->addItem(tr("1920x1152 (240 dpi)"), QSize(1920, 1152));
	if (landscape) uiResolutionCombo->addItem(tr("1920x1200 (240 dpi)"), QSize(1920, 1200));
	if (landscape) uiResolutionCombo->addItem(tr("2048x1536 (320 dpi)"), QSize(2048, 1536));
	if (landscape) uiResolutionCombo->addItem(tr("2560x1536 (320 dpi)"), QSize(2560, 1536));
	if (landscape) uiResolutionCombo->addItem(tr("2560x1600 (320 dpi)"), QSize(2560, 1600));

	uiResolutionCombo->addItem(tr("======= Tizen ======="));
	if (portrait) uiResolutionCombo->addItem(tr("480x800 (9:15)"), QSize(480, 800));
	if (landscape) uiResolutionCombo->addItem(tr("800x480 (15:9)"), QSize(800, 480));
	if (portrait) uiResolutionCombo->addItem(tr("720x1280 (9:16)"), QSize(720, 1280));
	if (landscape) uiResolutionCombo->addItem(tr("1280x720 (16:9)"), QSize(1280, 720));

	uiResolutionCombo->addItem(tr("=== Windows Phone ==="));
	if (portrait) uiResolutionCombo->addItem(tr("480x800 (WVGA, 9:15)"), QSize(480, 800));
	if (landscape) uiResolutionCombo->addItem(tr("800x480 (WVGA, 15:9)"), QSize(800, 480));
	if (portrait) uiResolutionCombo->addItem(tr("768x1280 (WXGA, 9:15)"), QSize(768, 1280));
	if (landscape) uiResolutionCombo->addItem(tr("1280x768 (WXGA, 15:9)"), QSize(1280, 768));
	if (portrait) uiResolutionCombo->addItem(tr("720x1280 (9:16)"), QSize(720, 1280));
	if (landscape) uiResolutionCombo->addItem(tr("1280x720 (16:9)"), QSize(1280, 720));
	if (portrait) uiResolutionCombo->addItem(tr("1080x1920 (9:16)"), QSize(1080, 1920));
	if (landscape) uiResolutionCombo->addItem(tr("1920x1080 (16:9)"), QSize(1920, 1080));

	if (landscape) uiResolutionCombo->addItem(tr("====== Desktop ======"));
	if (landscape) uiResolutionCombo->addItem(tr("640x480 (VGA, 4:3)"), QSize(640, 480));
	if (landscape) uiResolutionCombo->addItem(tr("800x600 (SVGA, 4:3)"), QSize(800, 600));
	if (landscape) uiResolutionCombo->addItem(tr("1024x600 (WSVGA, ~17:10)"), QSize(1024, 600));
	if (landscape) uiResolutionCombo->addItem(tr("1024x768 (XGA, 4:3)"), QSize(1024, 768));
	if (landscape) uiResolutionCombo->addItem(tr("1152x864 (XGA+, 4:3)"), QSize(1152, 864));
	if (landscape) uiResolutionCombo->addItem(tr("1280x720 (WXGA, 16:19)"), QSize(1280, 720));
	if (landscape) uiResolutionCombo->addItem(tr("1280x768 (WXGA, 5:3)"), QSize(1280, 768));
	if (landscape) uiResolutionCombo->addItem(tr("1280x800 (WXGA, 16:10)"), QSize(1280, 800));
	if (landscape) uiResolutionCombo->addItem(tr("1280x960 (UVGA, 4:3)"), QSize(1280, 960));
	if (landscape) uiResolutionCombo->addItem(tr("1280x1024 (SXGA, 5:4)"), QSize(1280, 1024));
	if (landscape) uiResolutionCombo->addItem(tr("1360x768 (HD, ~16:9)"), QSize(1360, 768));
	if (landscape) uiResolutionCombo->addItem(tr("1366x768 (HD, ~16:9)"), QSize(1366, 768));
	if (landscape) uiResolutionCombo->addItem(tr("1400x1050 (SXGA+, 4:3)"), QSize(1400, 1050));
	if (landscape) uiResolutionCombo->addItem(tr("1440x900 (WXGA+, 16:10)"), QSize(1440, 900));
	if (landscape) uiResolutionCombo->addItem(tr("1600x900 (HD+, 16:9)"), QSize(1600, 900));
	if (landscape) uiResolutionCombo->addItem(tr("1600x1200 (UXGA, 4:3)"), QSize(1600, 1200));
	if (landscape) uiResolutionCombo->addItem(tr("1680x1050 (WSXGA+, 16:10)"), QSize(1680, 1050));
	if (landscape) uiResolutionCombo->addItem(tr("1920x1200 (WUXGA, 16:10)"), QSize(1920, 1200));
	if (landscape) uiResolutionCombo->addItem(tr("2048x1152 (QWXGA, 16:9)"), QSize(2048, 1152));
	if (landscape) uiResolutionCombo->addItem(tr("2560x1440 (WQHD, 16:9)"), QSize(2560, 1440));
	if (landscape) uiResolutionCombo->addItem(tr("2560x1600 (WQXGA, 16:10)"), QSize(2560, 1600));

	if (landscape) uiResolutionCombo->addItem(tr("===== Television ====="));
	if (landscape) uiResolutionCombo->addItem(tr("720x480 (NTSC, 3:2)"), QSize(720, 480));
	if (landscape) uiResolutionCombo->addItem(tr("1280x720 (HD, 16:9)"), QSize(1280, 720));
	if (landscape) uiResolutionCombo->addItem(tr("1920x1080 (FullHD, 16:9)"), QSize(1920, 1080));

	uiResolutionCombo->addItem(tr("======================"));
	uiResolutionCombo->addItem(tr("Custom resolution..."), true);

	QSettings settings;
	int gameWidth = settings.value("gameWidth", landscape ? 1024 : 768).toInt();
	int gameHeight = settings.value("gameHeight", landscape ? 768 : 1024).toInt();
	int width = settings.value("width", landscape ? 1136 : 640).toInt();
	int height = settings.value("height", landscape ? 640 : 1136).toInt();
	setGameSize(QSize(width, height));
	setViewSize(QSize(width, height));

	uiWidthSpinBox->setValue(gameWidth);
	uiHeightSpinBox->setValue(gameHeight);

	int index = -1;
	for (int i = 0; i < uiResolutionCombo->count(); i++)
	{
		QVariant data = uiResolutionCombo->itemData(i);
		if (data.type() == QVariant::Bool)
		{
			index = i;
			continue;
		}

		QSize size = data.toSize();
		if (size.width() == width && size.height() == height)
		{
			index = i;
			break;
		}
	}
	uiResolutionCombo->setCurrentIndex(index);
}

MainWindow::~MainWindow()
{
}

void MainWindow::setGameSize(const QSize & size)
{
	m_GameSize = size;
	updateImage();

	QSettings settings;
	settings.setValue("gameWidth", size.width());
	settings.setValue("gameHeight", size.height());
}

void MainWindow::setViewSize(const QSize & size)
{
	m_ViewSize = size;
	updateImage();

	QSettings settings;
	settings.setValue("width", size.width());
	settings.setValue("height", size.height());
}

void MainWindow::on_uiWidthSpinBox_valueChanged(int)
{
	setGameSize(QSize(uiWidthSpinBox->value(), uiHeightSpinBox->value()));
}

void MainWindow::on_uiHeightSpinBox_valueChanged(int)
{
	setGameSize(QSize(uiWidthSpinBox->value(), uiHeightSpinBox->value()));
}

void MainWindow::on_uiResolutionCombo_currentIndexChanged(int index)
{
	if (index >= 0)
	{
		QVariant data = uiResolutionCombo->itemData(index);
		QSize size = data.toSize();
		if (size.width() > 0 && size.height() > 0)
			setViewSize(size);
		else if (data.type() == QVariant::Bool)
		{
			CustomResolutionDialog dlg(m_ViewSize.width(), m_ViewSize.height(), this);
			if (dlg.exec() == QDialog::Accepted)
				setViewSize(dlg.selectedSize());
		}
	}
}

void MainWindow::on_uiOpenButton_clicked()
{
	QString file = QFileDialog::getOpenFileName(this, QString(), QString(),
		tr("Images (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm *.tiff *.xbm *.xpm)"));
	if (file.length() != 0)
	{
		if (!m_Image.load(file))
		{
			QMessageBox::critical(this, tr("Error"), tr("Unable to load image '%1'.").arg(file));
			return;
		}
		uiImageView->setPixmap(QPixmap::fromImage(m_Image));
		updateImage();
	}
}

static inline QSize toSize(const vec2 & v)
{
	return QSize(static_cast<int>(v.x()), static_cast<int>(v.y()));
}

void MainWindow::updateImage()
{
	vec2 screenSize = vec2(static_cast<float>(m_ViewSize.width()), static_cast<float>(m_ViewSize.height()));
	vec2 gameSize = vec2(static_cast<float>(m_GameSize.width()), static_cast<float>(m_GameSize.height()));
	vec2 imageSize = vec2(static_cast<float>(m_Image.width()), static_cast<float>(m_Image.height()));

	vec2 innerSize = scaleInner(screenSize, gameSize);
	vec2 outerSize = scaleOuter(screenSize, imageSize);

	QSize inner = toSize(innerSize);
	QSize outer = toSize(outerSize);

	QSize full(qMax(outer.width(), m_ViewSize.width()), qMax(outer.height(), m_ViewSize.height()));
	uiScrollAreaContents->setFixedSize(full);

	if (outer.width() <= 0 || outer.height() <= 0 || m_Image.width() <= 0 || m_Image.height() <= 0)
		uiImageView->hide();
	else
	{
		int imageX = (full.width() - outer.width()) / 2;
		int imageY = (full.height() - outer.height()) / 2;
		uiImageView->move(imageX, imageY);
		uiImageView->setFixedSize(outer);
		uiImageView->show();
	}

	int screenX = (full.width() - m_ViewSize.width()) / 2;
	int screenY = (full.height() - m_ViewSize.height()) / 2;
	uiScreenFrame->move(screenX, screenY);
	uiScreenFrame->setFixedSize(m_ViewSize);

	int innerX = (full.width() - inner.width()) / 2;
	int innerY = (full.height() - inner.height()) / 2;
	uiInnerFrame->move(innerX, innerY);
	uiInnerFrame->setFixedSize(inner);

	uiStatusBar->showMessage(tr("Screen size: %1x%2   inner size: %3x%4   outer size: %5x%6")
		.arg(m_ViewSize.width()).arg(m_ViewSize.height())
		.arg(inner.width()).arg(inner.height())
		.arg(outer.width()).arg(outer.height())
	);
}
